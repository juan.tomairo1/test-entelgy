package com.restructureuser.testentelgy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestEntelgyApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestEntelgyApplication.class, args);
    }

}
